<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Registration;
use App\Repository\RegistrationRepository;

class CSVParser
{
    private RegistrationRepository $repository;

    /**
     * @param RegistrationRepository $repository
     */
    public function __construct(RegistrationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createCSV(): string
    {
        $registrations = $this->repository->findAll();

        $buffer = fopen('php://temp', 'r+');

        fputcsv($buffer, [
            'id', 'name', 'email', 'city', 'phone', 'created_at'
        ]);

        /** @var Registration $registration */
        foreach ($registrations as $registration) {
            $date = $registration->getCreatedAt();

            if ($date !== null) {
                $date = $date->format('Y-m-d H:i:s');
            }

            fputcsv($buffer, [
                $registration->getId(),
                $registration->getName(),
                $registration->getEmail(),
                $registration->getCity(),
                $registration->getPhone(),
                $date,
            ]);
        }
        rewind($buffer);
        $csv = stream_get_contents($buffer);
        fclose($buffer);

        return $csv;
    }
}
