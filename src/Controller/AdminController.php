<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\CSVParser;
use ReCaptcha\ReCaptcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin")
     */
    public function loginAction(): Response
    {
        return $this->render('admin/login.html.twig');
    }

    /**
     * @Route("/admin/csv")
     */
    public function downloadAction(Request $request, ReCaptcha $captcha, CSVParser $parser): Response
    {
        $token = $request->request->get('token');

        if (!$captcha->verify($token)) {
            return new JsonResponse('Wrong token!');
        }

        $password = $request->request->get('password');

        if ($password === getenv('ADMIN_PASSWORD')) {
            return $this->createFileResponse($parser->createCSV());
        }

        return new JsonResponse('Wrong password!');
    }

    private function createFileResponse(string $content): Response
    {
        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', 'text/csv' );
        $response->headers->set('Content-Disposition', 'attachment; filename="users.csv";');
        $response->headers->set('Content-length',  strlen($content));

        $response->sendHeaders();

        $response->setContent( $content );

        return $response;
    }
}
