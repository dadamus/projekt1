<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Registration;
use App\Repository\RegistrationRepository;
use ReCaptcha\ReCaptcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class LandingPageController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function indexAction(Session $session): Response
    {
        $flashBag = $session->getFlashBag();

        $flash = $flashBag->peekAll();
        $flashBag->clear();

        return $this->render('base.html.twig', [
            'flashBag' => $flash
        ]);
    }

    /**
     * @Route("/register", methods={"POST"})
     */
    public function registerAction(
        Request $request,
        ValidatorInterface $validator,
        RegistrationRepository $repository,
        ReCaptcha $captcha,
        Session $session
    ): Response {
        $token = $request->request->get('captcha_token');

        if (!$captcha->verify($token)) {
            throw new \Exception('Token not valid!');
        }

        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $city = $request->request->get('city');
        $phone = $request->request->get('phone');

        $registration = new Registration();
        $registration->setName($name);
        $registration->setEmail($email);
        $registration->setCity($city);
        $registration->setPhone($phone);
        $registration->setCreatedAt(new \DateTime());

        $errors = $validator->validate($registration);

        if ($errors->count() === 0) {
            $repository->save($registration);

            $session->getFlashBag()->add('success', 'Rejestracja udana!');
        } else {
            $session->getFlashBag()->add('error', 'Wystąpił błąd!');
        }

        return new RedirectResponse('/');
    }
}
